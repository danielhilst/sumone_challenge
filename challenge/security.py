import os
import hashlib

# Better security may be used here. But the database
# is local to the application. If an attacker has
# access to the database he has access to the application
# process too, so the salt is compromised. With
# the salt and the database he can brute force for the password
# hash. So have strong passwords!
salt = os.environ.get('SAFE_SALT', 'Pretty secret string!')

def passwd_hash(passwd):
    m = hashlib.sha256()
    m.update(passwd.encode('utf-8'))
    m.update(salt.encode('utf-8'))
    return m.hexdigest()
