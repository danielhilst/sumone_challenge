#!/usr/bin/env python3

import sys
import os
import re
from datetime import datetime
from PyQt5.QtWidgets import (
    QApplication, QWidget, QDialog, QLineEdit, QFormLayout,
    QPushButton, QMessageBox, QMainWindow, QLabel, QVBoxLayout,
    QTabWidget, QHBoxLayout, QTableWidget, QTableWidgetItem,
    QSizePolicy, QStatusBar
)
from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QIcon
from peewee import DoesNotExist

from .model import Operator, Purchase, cpf_validator
from .security import passwd_hash

if 'DEBUG' in os.environ:
    import logging
    logger = logging.getLogger('peewee')
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)

class Login(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.userlbl = QLabel('Operator:', self)
        self.passlbl = QLabel('Password:', self)
        self.username = QLineEdit(self) 
        self.password = QLineEdit(self)
        self.password.setEchoMode(QLineEdit.Password)
        self.login_btn = QPushButton('login', self)
        self.login_btn.clicked.connect(self.login_handle)
        layout = QFormLayout(self)
        layout.addRow(self.userlbl, self.username)
        layout.addRow(self.passlbl, self.password)
        layout.addRow(self.login_btn)
        self.setWindowTitle('Login')

    def login_handle(self):
        hash_ = passwd_hash(self.password.text())
        try:
            Operator.get(
                (Operator.name == self.username.text()) &
                (Operator.password == hash_))
            self.accept()
        except DoesNotExist:
            QMessageBox.warning(
                self, 'Error', 'Wrong user or password')

class Order(QWidget):
    def __init__(self, parent, operator, show_message):
        super().__init__(parent)
        self.operator = operator
        self.cpf = QLineEdit(self)
        self.value = QLineEdit(self)
        self.create = QPushButton('OK')
        self.clear = QPushButton('Clear')
        self.show_message = show_message

        self.create.clicked.connect(self.create_handle)
        self.clear.clicked.connect(self.clear_handle)

        layout = QFormLayout(self)
        layout.addRow(QLabel('CPF'), self.cpf)
        layout.addRow(QLabel('Valor'), self.value)
        layout.addRow(self.create, self.clear)

        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Ignored)
        self.setMinimumSize(420, 0)

    def create_handle(self):
        try:
            cpf = self.cpf.text()
            if cpf_validator(cpf):
                p = Purchase(
                    value=float(self.value.text().replace(',', '.')),
                    customer_cpf=self.cpf.text(),
                    operator=self.operator)
                p.save()
            else:
                raise ValueError('CPF inválido')
        except Exception as e:
            QMessageBox.warning(self, 'Error', str(e))
        else:
            self.show_message('Compra #{} registrada!'.format(p.id))
            self.clear_handle()

    def clear_handle(self):
        self.cpf.clear()
        self.value.clear()


class PurchasesWid(QWidget):


    def __init__(self, parent, show_message):
        super().__init__(parent)

        filterbar = QLineEdit()
        filterbar.setPlaceholderText(' filter')
        filterbar.returnPressed.connect(self.filterbar_handle)
        
        filterhelp = QPushButton('?')
        h = filterhelp.minimumSizeHint().height()
        filterhelp.setFixedSize(QSize(h,h))
        filterhelp.clicked.connect(self.show_help)

        filterrefresh = QPushButton('atualizar')
        filterrefresh.clicked.connect(self.filterbar_handle)

        filterlay = QHBoxLayout()
        filterlay.addWidget(filterhelp)
        filterlay.addWidget(filterbar)
        filterlay.addWidget(filterrefresh)
        filterwid = QWidget()
        filterwid.setLayout(filterlay)

        self.queryset = Purchase.select()
        self.tablewid = QTableWidget()
        self.tablewid.setMinimumSize(420, 200)
        self.refresh()
        
        layout = QVBoxLayout(self)
        layout.addWidget(filterwid)
        layout.addWidget(self.tablewid)

        self.filterbar = filterbar
        self.filterrefresh = filterrefresh
        self.show_message = show_message

    def filterbar_handle(self):
        ftr = self.filterbar.text()
        if ftr == '':
            self.queryset = Purchase.select()
            self.refresh()
            return

        datepattern = \
                r'^(?P<day>\d{1,2})/(?P<month>\d{1,2})/?(?P<year>\d{4})?$'
        datematch = re.match(datepattern, ftr)
        if datematch:
            y, m, d = datematch['year'], int(datematch['month']), int(datematch['day'])
            y = int(y) if y is not None else  datetime.now().year
            self.queryset = Purchase.select().where(
                    Purchase.date.startswith('{:04d}-{:02d}-{:02d}'.format(y,m,d)))
        
        valuepattern = r'^\d+(?:[.,]\d{1,2})?$'
        valuematch = re.match(valuepattern, ftr) 
        if valuematch is not None:
            self.queryset = Purchase.select().where(
                    Purchase.value >= float(ftr.replace(',', '.')))

        if (datematch is None and
                valuematch is None):
            self.queryset = Purchase.select().join(Operator).where(Operator.name.contains(ftr))
        
        count = self.queryset.count()
        if count > 0:
            self.show_message('{} resultados encontrados!'.format(self.queryset.count()))
        else:
            self.show_message('Nenhun resultado encontrado!')

        self.refresh()

    def refresh(self):
        self.tablewid.setColumnCount(4)
        self.tablewid.setRowCount(self.queryset.count())
        self.tablewid.setHorizontalHeaderLabels(
            ['Data', 'Valor', 'CPF', 'Operador'])
        for i, p in enumerate(self.queryset):
            self.tablewid.setItem(i, 0, QTableWidgetItem(str(p.date)))
            self.tablewid.setItem(i, 1, QTableWidgetItem(str(p.value)))
            self.tablewid.setItem(i, 2, QTableWidgetItem(p.customer_cpf))
            self.tablewid.setItem(i, 3, QTableWidgetItem(p.operator.name))

    def show_help(self):
        messagebox = QMessageBox()
        messagebox.setTextFormat(Qt.TextFormat.RichText)
        messagebox.setText('''
            <h3>Como usar o filtro?</h3>

            <p>O fitro de ajusta de acordo com a busca.  Para buscar uma
            <b>data</b> use o formato <code>dd/mm/yyyy</code>. O ano pode ser
            omitido. Nesse caso o ano atual é usado.</p>
            
            <p>Para buscar por um <b>valor</b> digite o preço desejado, no
            formato <code>d,dd</code> ou <code>d.dd</code>.  Nesse caso a busca
            retorna o que for igual ou maior que o preço buscado. Tanto o ponto
            como a virgula são suportados. A decima pode é opcional e pode ser
            omitida.</p>
            
            <p>Qualquer outro padrão vai buscar pelo operador.</p>''')
        messagebox.exec_()



class Tab(QWidget):
    def __init__(self, parent, operator):
        super().__init__(parent)
        self.layout = QVBoxLayout(self)

        self.statusbar = QStatusBar()
        self.statusbar.addPermanentWidget(QLabel(operator.name))

        def show_message(message):
            self.statusbar.showMessage(message, 3000)

        self.tabs = QTabWidget()
        self.tabs.currentChanged.connect(self.currentchanged_handle)
        self.order = Order(self, operator, show_message)
        self.order_sizehint = self.order.minimumSizeHint()
        self.purchases = PurchasesWid(self, show_message)
        self.purchases_sizehint = self.purchases.minimumSizeHint()
        self.tabs.addTab(self.order, 'Order')
        self.tabs.addTab(self.purchases, 'Purchases')
        self.adjust_size(self.order) # adjust tabs size

        
        self.layout.addWidget(self.tabs)
        self.layout.addWidget(self.statusbar)
        self.setLayout(self.layout)
    
    def adjust_size(self, wid):
        for i in range(self.tabs.count()):
            w = self.tabs.widget(i)
            if w is not wid:
                w.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)

        wid.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        wid.resize(wid.minimumSizeHint())
        wid.adjustSize()
        self.parent().resize(wid.minimumSizeHint())
        self.parent().adjustSize()

    def currentchanged_handle(self, index):
        self.adjust_size(self.tabs.widget(index))


class Window(QMainWindow):
    def __init__(self, operator):
        super().__init__()
        self.setWindowTitle('Challenge')
        self.move(50, 50)
        self.setCentralWidget(Tab(self, operator))
        self.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        path = os.path.join(os.path.abspath(__file__), '..', 'logo.png')
        print(path)
        self.setWindowIcon(QIcon(path))


class App(QApplication):
    def __init__(self):
        super().__init__(sys.argv)

def main():
    app = App()
    login = Login()
    login_status = login.exec_()
    if login_status == QDialog.Accepted:
        # save the operator 
        operator = Operator.get(Operator.name == login.username.text())
        window = Window(operator)
        window.show()
        sys.exit(app.exec_())
