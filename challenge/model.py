import re
from peewee import (
    Model, CharField, SqliteDatabase, ForeignKeyField,
    DateTimeField, DecimalField
)
from datetime import datetime

db = SqliteDatabase('challenge.db')

class BaseModel(Model):
    class Meta:
        database = db

class Operator(BaseModel):
    name = CharField(unique=True)
    password = CharField() # hashed

def cpf_validator(cpf):
    # @TODO, use peewee-validates here
    # This checks only the format
    # a better checking can be done by 
    # using the verification number
    CPF_PATTERN = r'\d{9}-\d\d'
    if cpf is None:
        return True
    if cpf == '':
        return True
    if re.match(CPF_PATTERN, cpf) is not None:
        return True
    return False

class Purchase(BaseModel):
    value = DecimalField()
    date = DateTimeField(default=datetime.now)
    customer_cpf = CharField()
    operator = ForeignKeyField(Operator, backref='purchases')

MODELS = (
    Operator,
    Purchase,
)
