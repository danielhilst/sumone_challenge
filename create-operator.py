from challenge.security import passwd_hash
from challenge.model import Operator, db, MODELS

if __name__ == '__main__':

    with db:
        db.create_tables(MODELS)

    username = input('Username: ')
    password = input('Password: ')
    with db:
        Operator(name=username, password=passwd_hash(password)).save()

