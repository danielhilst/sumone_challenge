from unittest import TestCase, main
from unittest.mock import patch, MagicMock
from datetime import datetime

from PyQt5.QtWidgets import (
    QDialog
)
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt, QTimer
from peewee import SqliteDatabase

test_db = SqliteDatabase(':memory:')

with patch('peewee.SqliteDatabase') as test_SqliteDatabase:
    test_SqliteDatabase.return_value = test_db
    from challenge.security import passwd_hash
    from challenge.model import db, Operator, Purchase, MODELS
    from challenge.ui import Login, App, Order, PurchasesWid

    class BaseTest(TestCase):
        def setUp(self):
            print('Creating tables')
            db.create_tables(MODELS)
            Operator(name='test',password=passwd_hash('test')).save()

        def tearDown(self):
            db.drop_tables(MODELS)

    
    class LoginTest(BaseTest):

        def test_login_dialog(self):
            app = App(); app # shut up pyflakes
            login = Login()
            QTest.keyClicks(login.username, 'test')
            QTest.keyClicks(login.password, 'test')
            QTimer.singleShot(0, lambda: QTest.mouseClick(login.login_btn, Qt.LeftButton))
            status = login.exec_()
            self.assertEqual(status, QDialog.Accepted)

    class OrderTest(BaseTest):
        def test_order(self):
            app = App(); app
            order = Order(None, Operator.get_by_id(1), lambda message: None)
            QTest.keyClicks(order.value, '1234')
            QTest.mouseClick(order.create, Qt.LeftButton)
            self.assertEqual(Purchase.get_by_id(1).value, 1234)

    class PurchaseTest(BaseTest):
        def setUp(self):
            super().setUp()
            Operator(name='testop', password=passwd_hash('test')).save()
            Purchase.insert_many([
                dict(value=1,
                     date=datetime(2018,1,1),
                     customer_cpf='887150361-97',
                     operator=Operator.get_by_id(1),
                ),
                dict(value=2,
                     date=datetime(2018,1,2),
                     customer_cpf='887150362-97',
                     operator=Operator.get_by_id(1),
                ),
                dict(value=3,
                     date=datetime(2018,1,3),
                     customer_cpf='887150363-97',
                     operator=Operator.get_by_id(1),
                ),
                dict(value=4,
                     date=datetime(2018,1,4),
                     customer_cpf='887150364-97',
                     operator=Operator.get_by_id(2),
                ),
            ]).execute()
            self.app = App()
            self.shwmsg_mock = MagicMock()
            self.purchasewid = PurchasesWid(None, self.shwmsg_mock)
            
        def test_search_by_value(self):
            QTest.keyClicks(self.purchasewid.filterbar, '3')
            QTest.mouseClick(self.purchasewid.filterrefresh, Qt.LeftButton)
            self.assertTrue(self.shwmsg_mock.called_with('2 resultados encontrados!'))

        def test_search_by_operator(self):
            QTest.keyClicks(self.purchasewid.filterbar, 'testop')
            QTest.mouseClick(self.purchasewid.filterrefresh, Qt.LeftButton)
            self.assertTrue(self.shwmsg_mock.called_with('1 resultados encontrados!'))

        def test_sarch_by_date(self):
            QTest.keyClicks(self.purchasewid.filterbar, '2/1/2018')
            QTest.mouseClick(self.purchasewid.filterrefresh, Qt.LeftButton)
            self.assertTrue(self.shwmsg_mock.called_with('1 resultados encontrados!'))
            

            



if __name__ == '__main__':
    main(verbosity=2)
