# Prepare

First, install python. I have tested with 3.6 on Linux and 3.7 on Windows 10.

After installing python create a virtual environment and activate it. To create
do `python -m venv`. You may need to user `python3 -m venv` if are on Linux
distro that hasn't updated default python interpreted yet.

Then, to activate, on windows do `venv\\Scripts\\activate.bat`, and on Linux,
`. venv/bin/activate`, assuming that you are using bash.

# Install

Clone this repository, then install the dependences with `pip install -r
requirements.txt`

# Create the operators

Run `python create-operator.py` to create new operators. It will ask you a
username and password. Once finished a new operator was created in the
database. Also this initialize the database if it doesn't exists.

# Run the application

Run `python main.py`

# Testing

Run `python test.py`

# Caveats

There are is a bug where the Order tab doesn't resize to it's default size.
This create a lot of empty space in the window. I could fix this on Linux but
the fix doesn't work on Windows.

There are a lot of queries happeing here at startup. I expected that the
`Purchases.select()` do *ONE* query. It may be a bad filtering or something in
the code.
